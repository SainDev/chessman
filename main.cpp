#include <windows.h>
#include <gl\old\gl.h>
#include <gl\old\glaux.h>

HWND hwnd;
HDC dc;
HGLRC hGLRC;
int figure;
unsigned int photo_tex;
AUX_RGBImageRec* photo_image;
GLUquadricObj *quadObj;
HMENU hMenu;
int alpha = 0;

int SetWindowPixelFormat(HDC hDC)
{
	int m_GLPixelIndex;
    PIXELFORMATDESCRIPTOR pfd;
    pfd.nSize       = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion    = 1;
    pfd.dwFlags   = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType     = PFD_TYPE_RGBA;
    pfd.cColorBits     = 32;
    pfd.cRedBits       = 8;
    pfd.cRedShift      = 16;
    pfd.cGreenBits     = 8;
    pfd.cGreenShift    = 8;
    pfd.cBlueBits      = 8;
    pfd.cBlueShift     = 0;
    pfd.cAlphaBits     = 0;
    pfd.cAlphaShift    = 0;
    pfd.cAccumBits     = 64;    
    pfd.cAccumRedBits  = 16;
    pfd.cAccumGreenBits   = 16;
    pfd.cAccumBlueBits    = 16;
    pfd.cAccumAlphaBits   = 0;
    pfd.cDepthBits        = 32;
    pfd.cStencilBits      = 8;
    pfd.cAuxBuffers       = 0;
    pfd.iLayerType        = PFD_MAIN_PLANE;
    pfd.bReserved         = 0;
    pfd.dwLayerMask       = 0;
    pfd.dwVisibleMask     = 0;
    pfd.dwDamageMask      = 0;
    m_GLPixelIndex = ChoosePixelFormat( hDC, &pfd);
    if(m_GLPixelIndex==0)
    {
		m_GLPixelIndex = 1;    
		if(DescribePixelFormat(hDC,m_GLPixelIndex,sizeof(PIXELFORMATDESCRIPTOR),&pfd)==0)
			return 0;
    }
    SetPixelFormat( hDC, m_GLPixelIndex, &pfd);
    return 1;
}

int Render(void)
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glLoadIdentity();
	glColor3d(1.,1.,1.);
	glRotated(120, 1, 0, 0);
	glRotated(alpha*20, 0, .4, 1);

	// ������ �����
	///////////////
	if (figure == 0) {
		glPushMatrix(); 
		quadObj = gluNewQuadric();
		gluQuadricDrawStyle(quadObj, GLU_FILL);
		gluCylinder(quadObj, .3, .6, 2, 20, 20);
		gluDeleteQuadric(quadObj);
		auxSolidTorus(0.1, 0.5);
		auxSolidTorus(0.1, 0.4);
		glTranslated(0, 0, -.2);
		auxSolidTorus(0.1, 0.3);
		glTranslated(0, 0, -.6);
		auxSolidSphere(.6);	
		glTranslated(0,0,2.6);
		auxSolidTorus(0.2, .7);
		glTranslated(0,0,.3);
		auxSolidTorus(0.2, .7);
		glTranslated(0,0,.2);
		glColor3d(1,0,0);
		quadObj = gluNewQuadric();
		gluDisk(quadObj,0, .6, 20, 20);
		gluDeleteQuadric(quadObj);
		glPopMatrix();
	}
	//����� �����
	/////////////
	// ������
	if (figure == 1) {
		quadObj = gluNewQuadric();
		gluQuadricDrawStyle(quadObj, GLU_FILL);
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		glPushMatrix(); 
		gluCylinder(quadObj, .2, .7, 3.2, 20, 20);  
		glTranslated(0,0,.4);
		glTranslated(0,0,-.2);
		gluDisk(quadObj, 0, .7, 15, 15);
		glTranslated(0,0,-.1);
		auxSolidTorus(.1, .7);
		glTranslated(0,0,-.1);
		gluDisk(quadObj, 0, .7, 15, 15);
		glTranslated(0,0,-.1);
		auxSolidTorus(.1, .4); 
		glTranslated(0,0,-.2);
		auxSolidTorus(.1, .4); 
		glTranslated(0,0,-.6);
		gluCylinder(quadObj, .7, .3, .6, 20, 20);  
		glTranslated(0,0,-.6);
		gluCylinder(quadObj, .3, .7, .6, 20, 20);  
		glTranslated(0,0,-.2);
		gluCylinder(quadObj, .3, .3, .3, 20, 20); 
		auxSolidTorus(.1,.2);
		glTranslated(0,0,-.1);
		gluDisk(quadObj, 0, .2, 15, 15);
		glTranslated(0,0,3.7);
		auxSolidTorus(0.2, .6);
		glTranslated(0,0,.6);
		auxSolidTorus(0.3, .8);
		auxSolidTorus(0.3, .7);
		glTranslated(0,0,.4);
		auxSolidTorus(0.3,.8);
		glTranslated(0,0,.3);
		glColor3d(1,0,0);
		gluDisk(quadObj,0, .8, 20, 20);
	}
	//����� �������
	///////////////
	//������ �����
	////////////////
	if (figure == 2) {
		quadObj = gluNewQuadric();
		gluQuadricDrawStyle(quadObj, GLU_FILL);
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		glPushMatrix(); 
		gluCylinder(quadObj, .5, .6, 2.3, 20, 20);  
		glTranslated(0,0,.4);
		glTranslated(0,0,-.3);
		auxSolidTorus(.1, .6); 
		glTranslated(0,0,-.3);
		auxSolidTorus(.2, .6); 
		glTranslated(0,0,-.5);
		gluCylinder(quadObj, .8, .5, .4, 20, 20); 
		gluDisk(quadObj, 0, .8, 15, 15);
		glTranslated(0,-.6,-.1);
		auxSolidBox(.3, .3, .2);
		glTranslated(0,1.2,0);
		auxSolidBox(.3, .3, .2);
		glTranslated(0,-.6,0);
		glTranslated(.6,0,0);
		auxSolidBox(.3, .3, .2);
		glTranslated(-1.2,0,0);
		auxSolidBox(.3, .3, .2);
		glTranslated(.6, 0,0);
		glTranslated(0,0,2.8);
		auxSolidTorus(0.2, .6);
		glTranslated(0,0,.6);
		auxSolidTorus(0.3, .8);
		auxSolidTorus(0.3, .7);
		glTranslated(0,0,.4);
		auxSolidTorus(0.3,.8);
		glTranslated(0,0,.3);
		glColor3d(1,0,0);
		gluDisk(quadObj,0, .8, 20, 20);
	}
	//����� �����
	// ������
	if (figure == 3) {
		quadObj = gluNewQuadric();
		gluQuadricDrawStyle(quadObj, GLU_FILL);
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		glPushMatrix(); 
		gluCylinder(quadObj, .2, .7, 2.6, 20, 20);  
		glTranslated(0,0,.4);
		glTranslated(0,0,-.2);
		gluDisk(quadObj, 0, .7, 15, 15);
		glTranslated(0,0,-.1);
		auxSolidTorus(.1, .7);
		glTranslated(0,0,-.1);
		gluDisk(quadObj, 0, .7, 15, 15);
		glTranslated(0,0,-.1);
		auxSolidTorus(.1, .4); 
		glTranslated(0,0,-.2);
		auxSolidTorus(.1, .4); 
		glTranslated(0,0,-.8);
		gluCylinder(quadObj, .7, .3, 1, 20, 20); 
		gluDisk(quadObj, 0, .7, 15, 15);
		glTranslated(0,0,-.1);
		auxSolidTorus(.1, .3);
		glTranslated(0,0,-.1);
		gluDisk(quadObj, 0, .3, 15, 15);		
		glTranslated(0,0,-.6);
		auxSolidBox(.3, .3, 1);
		auxSolidBox(.3, .6, .3);
		glTranslated(0,0,4);
		auxSolidTorus(0.2, .6);
		glTranslated(0,0,.6);
		auxSolidTorus(0.3, .8);
		auxSolidTorus(0.3, .7);
		glTranslated(0,0,.4);
		auxSolidTorus(0.3,.8);
		glTranslated(0,0,.3);
		glColor3d(1,0,0);
		gluDisk(quadObj,0, .8, 20, 20);
	}
	//����� ������
	///////////////
	// ������
	if (figure == 4) {
		quadObj = gluNewQuadric();
		gluQuadricDrawStyle(quadObj, GLU_FILL);
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		glPushMatrix(); 
		gluCylinder(quadObj, .2, .7, 2.6, 20, 20);  
		glTranslated(0,0,.4);
		glTranslated(0,0,-.2);
		gluDisk(quadObj, 0, .7, 15, 15);
		glTranslated(0,0,-.1);
		auxSolidTorus(.1, .7);
		glTranslated(0,0,-.1);
		gluDisk(quadObj, 0, .7, 15, 15);
		glTranslated(0,0,-.1);
		auxSolidTorus(.1, .4); 
		glTranslated(0,0,-.2);
		auxSolidTorus(.1, .4); 
		glTranslated(0,0,-.8);
		gluCylinder(quadObj, .7, .3, 1, 20, 20); 
		glTranslated(0,0,-.4);
		gluCylinder(quadObj, .2, .7, .4, 20, 20);
		glTranslated(0,0,-.1);
		auxSolidTorus(.1,.2);
		glTranslated(0,0,-.3);
		auxSolidIcosahedron(.3);
		glTranslated(0,0,4);
		auxSolidTorus(0.2, .6);
		glTranslated(0,0,.6);
		auxSolidTorus(0.3, .8);
		auxSolidTorus(0.3, .7);
		glTranslated(0,0,.4);
		auxSolidTorus(0.3,.8);
		glTranslated(0,0,.3);
		glColor3d(1,0,0);
		gluDisk(quadObj,0, .8, 20, 20);
	}
	//����� ������
	///////////////
	SwapBuffers(wglGetCurrentDC());
	return 0;
}

LRESULT CALLBACK MainWinProc(HWND   hwnd,  UINT    msg, WPARAM  wparam, LPARAM  lparam)
{
	switch(msg)
	{
		case WM_COMMAND:
		{
			if ((LOWORD(wparam)>=100)&&(LOWORD(wparam)<200)) figure = LOWORD(wparam) - 101;
			if ((LOWORD(wparam)>=200)&&(LOWORD(wparam)<=300)){
				CHAR buf[200];
				GetMenuString(hMenu, LOWORD(wparam), buf, sizeof(buf), MF_BYCOMMAND);
				photo_image = auxDIBImageLoad(buf);
				glTexImage2D(GL_TEXTURE_2D, 0, 3, photo_image->sizeX,
							 photo_image->sizeY, 0, GL_RGB, GL_UNSIGNED_BYTE,
							 photo_image->data);
			}
			return 0;
		}

		case WM_CREATE:
		{
			dc = GetDC(hwnd);
		    SetWindowPixelFormat(dc);
		    hGLRC = wglCreateContext(dc);
		    wglMakeCurrent(dc, hGLRC);

		    glEnable(GL_ALPHA_TEST);
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_COLOR_MATERIAL);
			glEnable(GL_LIGHTING);
			glEnable(GL_LIGHT0);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			float pos[4] = {2,2,2,1};
			float spec[4] = {1,1,1,1};
			float dir[3] = {-1,-1,-1};
			glLightfv(GL_LIGHT0, GL_POSITION, pos);
			glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, dir);
			glLightfv(GL_LIGHT0, GL_SPECULAR, spec);

			float ambient[4] = {0.4, 0.4, 0.4, 1};
			glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient);

			photo_image = auxDIBImageLoad("texture.bmp");
			glTexImage2D(GL_TEXTURE_2D, 0, 3, 
				photo_image->sizeX,
                 photo_image->sizeY,
                 0, GL_RGB, GL_UNSIGNED_BYTE,
                 photo_image->data);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

			glEnable(GL_TEXTURE_2D);
			glEnable(GL_TEXTURE_GEN_S);
			glEnable(GL_TEXTURE_GEN_T);
   			glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
			glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);



			figure = 4;		
			return 0;
		}

		case WM_SIZE:
		{		
			glViewport(0,0,HIWORD(lparam),LOWORD(lparam));
			glMatrixMode( GL_PROJECTION );
			glLoadIdentity();
			glOrtho(-5,5, -5,5, -10,10);   
			gluLookAt( 0,0,5, 0,0,0, 0,1,0 );
			glMatrixMode( GL_MODELVIEW );
			return 0;
		}
		case WM_DESTROY:
		{
			PostMessage(hwnd, WM_QUIT, 0, 0);
			return 0;
		}

		case WM_TIMER:
		{
			alpha = alpha++ % 360;
			Render();
			return 0;
		}

	} 
	return DefWindowProc(hwnd, msg, wparam, lparam);
}

int MenuCreate(HINSTANCE hinstance)
{
	

	hMenu = CreateMenu();
	SetMenu(hwnd, hMenu);
	HMENU Figures = CreateMenu();
	AppendMenu(Figures, MF_STRING, 101, "Pawn");
	AppendMenu(Figures, MF_STRING, 102, "Bishop");
	AppendMenu(Figures, MF_STRING, 103, "Rook");
	AppendMenu(Figures, MF_STRING, 104, "King");
	AppendMenu(Figures, MF_STRING, 105, "Queen");
	AppendMenu(hMenu , MF_POPUP, (UINT) Figures, "Figures");

	HMENU Texture = CreateMenu();
	int i = 200;

	WIN32_FIND_DATA fd;
	HANDLE fh;
	if ((fh = FindFirstFile("*.bmp", &fd))==NULL)
		AppendMenu(Texture, MF_STRING, 200, "Texture NotFound"); else{
			AppendMenu(Texture, MF_STRING, 200, fd.cFileName);
			while (FindNextFile(fh, &fd)){
				i++;
				AppendMenu(Texture, MF_STRING, i, fd.cFileName);
			}
		}
	AppendMenu(hMenu , MF_POPUP, (UINT)Texture, "Textures");	

	return 0;
}

int WINAPI WinMain(	HINSTANCE hinstance, HINSTANCE hprevinstance,
					LPSTR lpcmdline, int ncmdshow)
{	
	WNDCLASSEX windowsclass;  
	MSG        msg;           
	windowsclass.cbSize         = sizeof(WNDCLASSEX);
	windowsclass.style			= CS_GLOBALCLASS | CS_HREDRAW | CS_VREDRAW; //CS_DBLCLKS | CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
	windowsclass.lpfnWndProc	= MainWinProc;
	windowsclass.cbClsExtra		= 0;
	windowsclass.cbWndExtra		= 0;
	windowsclass.hInstance		= hinstance;
	windowsclass.hIcon			= LoadIcon(NULL, IDI_APPLICATION);
	windowsclass.hCursor		= LoadCursor(NULL, IDC_ARROW);
	windowsclass.hbrBackground	= (HBRUSH)GetStockObject(GRAY_BRUSH);
	windowsclass.lpszMenuName	=  NULL;
	windowsclass.lpszClassName	= "CHESSCLASS";
	windowsclass.hIconSm        = LoadIcon(NULL, IDI_APPLICATION);
	RegisterClassEx(&windowsclass);

	hwnd = CreateWindow("CHESSCLASS",         
					   "Chess", WS_OVERLAPPEDWINDOW,
					   	0, 0, 600, 600,
					   NULL, NULL, hinstance, NULL);
	SetTimer(hwnd, 3, 70, NULL);	
	MenuCreate(hinstance);

	ShowWindow( hwnd, SW_SHOWDEFAULT );
	UpdateWindow( hwnd );
	while (GetMessage(&msg, NULL, 0, 0)){
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return 0;
}
