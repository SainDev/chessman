/**************************
 * Includes
 *
 **************************/

#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include "glaux.h"

/**************************
 * Function Declarations
 *
 **************************/

LRESULT CALLBACK WndProc (HWND hWnd, UINT message,
WPARAM wParam, LPARAM lParam);
void EnableOpenGL (HWND hWnd, HDC *hDC, HGLRC *hRC);
void DisableOpenGL (HWND hWnd, HDC hDC, HGLRC hRC);

HWND hWnd;
HDC hDC;
HGLRC hRC;
int figure;
unsigned int photo_tex;
AUX_RGBImageRec* photo_image;
GLUquadricObj *quadObj;
HMENU menu;
int alpha = 0;

float	xrot = 0.0f;			// �������� �� ��� X
float	yrot = 0.0f;			// Y
float	zrot = 0.0f;            // Z

int Render(void)
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glLoadIdentity();
	glColor3d(1.,1.,1.);
	//glRotated(120, 1, 0, 0);
	//glRotated(alpha*20, 0, .4, 1);
	
	xrot+=2.0f;
	yrot+=5.0f;
	zrot+=8.0f;
    glRotatef(xrot,0.0f,0.0f,1.0f);
	glRotatef(yrot,0.0f,2.0f,0.0f);
	glRotatef(zrot,1.0f,0.0f,0.0f);

	// ������ �����
	///////////////
	if (figure == 0) {
		glPushMatrix(); 
		quadObj = gluNewQuadric();
		gluQuadricDrawStyle(quadObj, GLU_FILL);
		gluCylinder(quadObj, .3, .6, 2, 20, 20);
		gluDeleteQuadric(quadObj);
		auxSolidTorus(0.1, 0.5);
		auxSolidTorus(0.1, 0.4);
		glTranslated(0, 0, -.2);
		auxSolidTorus(0.1, 0.3);
		glTranslated(0, 0, -.6);
		auxSolidSphere(.6);	
		glTranslated(0,0,2.6);
		auxSolidTorus(0.2, .7);
		glTranslated(0,0,.3);
		auxSolidTorus(0.2, .7);
		glTranslated(0,0,.2);
		glColor3d(1,0,0);
		quadObj = gluNewQuadric();
		gluDisk(quadObj,0, .6, 20, 20);
		gluDeleteQuadric(quadObj);
		glPopMatrix();
	}
	//����� �����
	/////////////
	// ������
	if (figure == 1) {
		quadObj = gluNewQuadric();
		gluQuadricDrawStyle(quadObj, GLU_FILL);
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		glPushMatrix(); 
		gluCylinder(quadObj, .2, .7, 3.2, 20, 20);  
		glTranslated(0,0,.4);
		glTranslated(0,0,-.2);
		gluDisk(quadObj, 0, .7, 15, 15);
		glTranslated(0,0,-.1);
		auxSolidTorus(.1, .7);
		glTranslated(0,0,-.1);
		gluDisk(quadObj, 0, .7, 15, 15);
		glTranslated(0,0,-.1);
		auxSolidTorus(.1, .4); 
		glTranslated(0,0,-.2);
		auxSolidTorus(.1, .4); 
		glTranslated(0,0,-.6);
		gluCylinder(quadObj, .7, .3, .6, 20, 20);  
		glTranslated(0,0,-.6);
		gluCylinder(quadObj, .3, .7, .6, 20, 20);  
		glTranslated(0,0,-.2);
		gluCylinder(quadObj, .3, .3, .3, 20, 20); 
		auxSolidTorus(.1,.2);
		glTranslated(0,0,-.1);
		gluDisk(quadObj, 0, .2, 15, 15);
		glTranslated(0,0,3.7);
		auxSolidTorus(0.2, .6);
		glTranslated(0,0,.6);
		auxSolidTorus(0.3, .8);
		auxSolidTorus(0.3, .7);
		glTranslated(0,0,.4);
		auxSolidTorus(0.3,.8);
		glTranslated(0,0,.3);
		glColor3d(1,0,0);
		gluDisk(quadObj,0, .8, 20, 20);
	}
	//����� �������
	///////////////
	//������ �����
	////////////////
	if (figure == 2) {
		quadObj = gluNewQuadric();
		gluQuadricDrawStyle(quadObj, GLU_FILL);
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		glPushMatrix(); 
		gluCylinder(quadObj, .5, .6, 2.3, 20, 20);  
		glTranslated(0,0,.4);
		glTranslated(0,0,-.3);
		auxSolidTorus(.1, .6); 
		glTranslated(0,0,-.3);
		auxSolidTorus(.2, .6); 
		glTranslated(0,0,-.5);
		gluCylinder(quadObj, .8, .5, .4, 20, 20); 
		gluDisk(quadObj, 0, .8, 15, 15);
		glTranslated(0,-.6,-.1);
		auxSolidBox(.3, .3, .2);
		glTranslated(0,1.2,0);
		auxSolidBox(.3, .3, .2);
		glTranslated(0,-.6,0);
		glTranslated(.6,0,0);
		auxSolidBox(.3, .3, .2);
		glTranslated(-1.2,0,0);
		auxSolidBox(.3, .3, .2);
		glTranslated(.6, 0,0);
		glTranslated(0,0,2.8);
		auxSolidTorus(0.2, .6);
		glTranslated(0,0,.6);
		auxSolidTorus(0.3, .8);
		auxSolidTorus(0.3, .7);
		glTranslated(0,0,.4);
		auxSolidTorus(0.3,.8);
		glTranslated(0,0,.3);
		glColor3d(1,0,0);
		gluDisk(quadObj,0, .8, 20, 20);
	}
	//����� �����
	// ������
	if (figure == 3) {
		quadObj = gluNewQuadric();
		gluQuadricDrawStyle(quadObj, GLU_FILL);
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		glPushMatrix(); 
		gluCylinder(quadObj, .2, .7, 2.6, 20, 20);  
		glTranslated(0,0,.4);
		glTranslated(0,0,-.2);
		gluDisk(quadObj, 0, .7, 15, 15);
		glTranslated(0,0,-.1);
		auxSolidTorus(.1, .7);
		glTranslated(0,0,-.1);
		gluDisk(quadObj, 0, .7, 15, 15);
		glTranslated(0,0,-.1);
		auxSolidTorus(.1, .4); 
		glTranslated(0,0,-.2);
		auxSolidTorus(.1, .4); 
		glTranslated(0,0,-.8);
		gluCylinder(quadObj, .7, .3, 1, 20, 20); 
		gluDisk(quadObj, 0, .7, 15, 15);
		glTranslated(0,0,-.1);
		auxSolidTorus(.1, .3);
		glTranslated(0,0,-.1);
		gluDisk(quadObj, 0, .3, 15, 15);		
		glTranslated(0,0,-.6);
		auxSolidBox(.3, .3, 1);
		auxSolidBox(.3, .6, .3);
		glTranslated(0,0,4);
		auxSolidTorus(0.2, .6);
		glTranslated(0,0,.6);
		auxSolidTorus(0.3, .8);
		auxSolidTorus(0.3, .7);
		glTranslated(0,0,.4);
		auxSolidTorus(0.3,.8);
		glTranslated(0,0,.3);
		glColor3d(1,0,0);
		gluDisk(quadObj,0, .8, 20, 20);
	}
	//����� ������
	///////////////
	// ������
	if (figure == 4) {
		quadObj = gluNewQuadric();
		gluQuadricDrawStyle(quadObj, GLU_FILL);
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		glPushMatrix(); 
		gluCylinder(quadObj, .2, .7, 2.6, 20, 20);  
		glTranslated(0,0,.4);
		glTranslated(0,0,-.2);
		gluDisk(quadObj, 0, .7, 15, 15);
		glTranslated(0,0,-.1);
		auxSolidTorus(.1, .7);
		glTranslated(0,0,-.1);
		gluDisk(quadObj, 0, .7, 15, 15);
		glTranslated(0,0,-.1);
		auxSolidTorus(.1, .4); 
		glTranslated(0,0,-.2);
		auxSolidTorus(.1, .4); 
		glTranslated(0,0,-.8);
		gluCylinder(quadObj, .7, .3, 1, 20, 20); 
		glTranslated(0,0,-.4);
		gluCylinder(quadObj, .2, .7, .4, 20, 20);
		glTranslated(0,0,-.1);
		auxSolidTorus(.1,.2);
		glTranslated(0,0,-.3);
		auxSolidIcosahedron(.3);
		glTranslated(0,0,4);
		auxSolidTorus(0.2, .6);
		glTranslated(0,0,.6);
		auxSolidTorus(0.3, .8);
		auxSolidTorus(0.3, .7);
		glTranslated(0,0,.4);
		auxSolidTorus(0.3,.8);
		glTranslated(0,0,.3);
		glColor3d(1,0,0);
		gluDisk(quadObj,0, .8, 20, 20);
	}
	//����� ������
	///////////////
	SwapBuffers(wglGetCurrentDC());
	return 0;
}

int MenuCreate(HINSTANCE hinstance)
{
	

	menu = CreateMenu();
	//SetMenu(hWnd, menu);
	HMENU Figures = CreateMenu();
	AppendMenu(Figures, MF_STRING, 101, "Pawn");
	AppendMenu(Figures, MF_STRING, 102, "Bishop");
	AppendMenu(Figures, MF_STRING, 103, "Rook");
	AppendMenu(Figures, MF_STRING, 104, "King");
	AppendMenu(Figures, MF_STRING, 105, "Queen");
	AppendMenu(menu , MF_POPUP, (UINT) Figures, "Figures");

	HMENU Texture = CreateMenu();
	int i = 200;

	WIN32_FIND_DATA fd;
	HANDLE fh;
	if ((fh = FindFirstFile("*.bmp", &fd))==NULL)
		AppendMenu(Texture, MF_STRING, 200, "Texture NotFound"); else{
			AppendMenu(Texture, MF_STRING, 200, fd.cFileName);
			while (FindNextFile(fh, &fd)){
				i++;
				AppendMenu(Texture, MF_STRING, i, fd.cFileName);
			}
		}
	AppendMenu(menu , MF_POPUP, (UINT)Texture, "Textures");	

	return 0;
}

/**************************
 * WinMain
 *
 **************************/

int WINAPI WinMain (HINSTANCE hInstance,
                    HINSTANCE hPrevInstance,
                    LPSTR lpCmdLine,
                    int iCmdShow)
{
    WNDCLASS wc;
    MSG msg;
    BOOL bQuit = FALSE;

    /* register window class */
    wc.style = CS_OWNDC;
    wc.lpfnWndProc = WndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = hInstance;
    wc.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wc.hCursor = LoadCursor (NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH) GetStockObject (LTGRAY_BRUSH);
    wc.lpszMenuName = NULL;
    wc.lpszClassName = "GLSample";
    RegisterClass (&wc);

    /* create main window */
    hWnd = CreateWindow (
      "GLSample", "OpenGL Sample", 
      WS_CAPTION | WS_POPUPWINDOW | WS_VISIBLE,
      0, 0, 600, 600,
      NULL, NULL, hInstance, NULL);
      
    SetTimer(hWnd, 3, 70, NULL);
    
    //menu = LoadMenu(hInstance, MAKEINTRESOURCE(ID_MENU));
    MenuCreate(hInstance);
    SetMenu(hWnd, menu);
    
    ShowWindow( hWnd, SW_SHOWDEFAULT );
	UpdateWindow( hWnd );

    /* enable OpenGL for the window */
    EnableOpenGL (hWnd, &hDC, &hRC);

    /* program main loop */
    while (!bQuit)
    {
        /* check for messages */
        if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
        {
            /* handle or dispatch messages */
            if (msg.message == WM_QUIT)
            {
                bQuit = TRUE;
            }
            else
            {
                TranslateMessage (&msg);
                DispatchMessage (&msg);
            }
        }
        /*else
        {

            SwapBuffers (hDC);

            theta += 1.0f;
            Sleep (1);
        }*/
    }
    
    /* shutdown OpenGL */
    DisableOpenGL (hWnd, hDC, hRC);

    /* destroy the window explicitly */
    DestroyWindow (hWnd);

    return msg.wParam;
}


/********************
 * Window Procedure
 *
 ********************/

LRESULT CALLBACK WndProc (HWND hWnd, UINT message,
                          WPARAM wParam, LPARAM lParam)
{

    switch (message)
    {
        case WM_COMMAND:
        {
			if ((LOWORD(wParam)>=100)&&(LOWORD(wParam)<200)) figure = LOWORD(wParam) - 101;
			if ((LOWORD(wParam)>=200)&&(LOWORD(wParam)<=300)){
				CHAR buf[200];
				GetMenuString(menu, LOWORD(wParam), buf, sizeof(buf), MF_BYCOMMAND);
				photo_image = auxDIBImageLoad(buf);
				glTexImage2D(GL_TEXTURE_2D, 0, 3, photo_image->sizeX,
							 photo_image->sizeY, 0, GL_RGB, GL_UNSIGNED_BYTE,
							 photo_image->data);
			}
			return 0;
		}

        case WM_CREATE:		
			 return 0;
             
        case WM_CLOSE:
             PostQuitMessage (0);
             return 0;

        case WM_DESTROY:
             return 0;
             
        case WM_TIMER:
		{
			alpha = alpha++ % 360;
			Render();
			return 0;
		}

        case WM_KEYDOWN:
             switch (wParam)
             {
                case VK_ESCAPE:
                     PostQuitMessage(0);
                     return 0;
             }
             return 0;

             default:
                     return DefWindowProc (hWnd, message, wParam, lParam);
    }
}


/*******************
 * Enable OpenGL
 *
 *******************/

void EnableOpenGL (HWND hWnd, HDC *hDC, HGLRC *hRC)
{
    PIXELFORMATDESCRIPTOR pfd;
    int iFormat;

    /* get the device context (DC) */
    *hDC = GetDC (hWnd);

    /* set the pixel format for the DC */
    ZeroMemory (&pfd, sizeof (pfd));
    pfd.nSize = sizeof (pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | 
      PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 24;
    pfd.cRedBits       = 8;
    pfd.cRedShift      = 16;
    pfd.cGreenBits     = 8;
    pfd.cGreenShift    = 8;
    pfd.cBlueBits      = 8;
    pfd.cBlueShift     = 0;
    pfd.cAlphaBits     = 0;
    pfd.cAlphaShift    = 0;
    pfd.cAccumBits     = 64;    
    pfd.cAccumRedBits  = 16;
    pfd.cAccumGreenBits   = 16;
    pfd.cAccumBlueBits    = 16;
    pfd.cAccumAlphaBits   = 0;
    pfd.cDepthBits = 16;
    pfd.cStencilBits      = 8;
    pfd.cAuxBuffers       = 0;
    pfd.iLayerType = PFD_MAIN_PLANE;
    pfd.bReserved         = 0;
    pfd.dwLayerMask       = 0;
    pfd.dwVisibleMask     = 0;
    pfd.dwDamageMask      = 0;
    iFormat = ChoosePixelFormat (*hDC, &pfd);
    if(iFormat == 0) iFormat = 1;
    SetPixelFormat (*hDC, iFormat, &pfd);

    /* create and enable the render context (RC) */
    *hRC = wglCreateContext( *hDC );
    wglMakeCurrent( *hDC, *hRC );
    
    glEnable(GL_ALPHA_TEST);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    float pos[4] = {2,2,2,1};
    float spec[4] = {1,1,1,1};
    float dir[3] = {-1,-1,-1};
    glLightfv(GL_LIGHT0, GL_POSITION, pos);
    glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, dir);
    glLightfv(GL_LIGHT0, GL_SPECULAR, spec);
    
    float ambient[4] = {0.4, 0.4, 0.4, 1};
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient);
    
    photo_image = auxDIBImageLoad("00.bmp");
			glTexImage2D(GL_TEXTURE_2D, 0, 3, 
				photo_image->sizeX,
                 photo_image->sizeY,
                 0, GL_RGB, GL_UNSIGNED_BYTE,
                 photo_image->data);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    
    /*glEnable(GL_TEXTURE_2D);
    glEnable(GL_TEXTURE_GEN_S);
    glEnable(GL_TEXTURE_GEN_T);
    glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
    glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);*/
    
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    glOrtho(-5,5, -5,5, -10,10);   
    gluLookAt( 0,0,5, 0,0,0, 0,1,0 );
    glMatrixMode( GL_MODELVIEW );
    
    
	
}

/******************
 * Disable OpenGL
 *
 ******************/

void DisableOpenGL (HWND hWnd, HDC hDC, HGLRC hRC)
{
    wglMakeCurrent (NULL, NULL);
    wglDeleteContext (hRC);
    ReleaseDC (hWnd, hDC);
}
